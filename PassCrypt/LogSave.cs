﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassCrypt
{
    public partial class LogSave : Form
    {
        public LogSave()
        {
            InitializeComponent();
        }

        private void LogSave_Load(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-N2I1H66 ; Initial Catalog=PassBox; Integrated Security=True");
            string sql = "select logID,Date from login_log where Username=@u";
            SqlCommand con = new SqlCommand(sql, sqlCon);
            con.Parameters.AddWithValue("@u", txtGotUser.Text);
            sqlCon.Open();
            SqlDataAdapter sqda = new SqlDataAdapter(con);
            DataTable dt = new DataTable();
            sqda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
