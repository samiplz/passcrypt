﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PassCrypt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void addNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewUser addnewUser = new AddNewUser();
            addnewUser.Show();
            this.Hide();
            
        }
        public string userNameSetter;
        private void btnLogin_Click(object sender, EventArgs e)
        {
            
            SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-N2I1H66 ; Initial Catalog=PassBox; Integrated Security=True");
            string sql = "Select * from userInfo where UserName=@u and Password=@p";
            SqlCommand con = new SqlCommand(sql,sqlCon);
            con.Parameters.AddWithValue("@u", txtUsername.Text);
            con.Parameters.AddWithValue("@p",txtPass.Text);
            try
            {
                
                sqlCon.Open();
                SqlDataAdapter sqda = new SqlDataAdapter(con);
                DataSet dataset = new DataSet();
                sqda.Fill(dataset);
                sqlCon.Close();
                int i = dataset.Tables[0].Rows.Count;
                if (i == 1)
                {
                    MainLogin ml = new MainLogin();
                    ml.Show();
                    ml.Text = "Logged in as : " + txtUsername.Text;
                    ml.textBox1.Text = txtUsername.Text;
                    ml.textBox2.Text = txtUsername.Text;
                    this.Hide();
                    
                    
                }else
                {
                    MessageBox.Show("Login Failed . Check Username & Password");
                    
                    txtUsername.Focus();
                }

            }
            catch(Exception couldntconnect)
            {
                MessageBox.Show("Couldn't Connect");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
