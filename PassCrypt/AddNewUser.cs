﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassCrypt
{
    public partial class AddNewUser : Form
    {
        public AddNewUser()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-N2I1H66 ; Initial Catalog=PassBox; Integrated Security=True");
            string sql = "insert into userInfo(username,password,Name) values(@u,@p,@n)";
            SqlCommand con = new SqlCommand(sql, sqlCon);
            con.Parameters.AddWithValue("@u",txtUsername.Text);
            con.Parameters.AddWithValue("@p",txtPass.Text);
            con.Parameters.AddWithValue("@n",txtFullName.Text);
            sqlCon.Open();
            try
            {
                int i = con.ExecuteNonQuery();

                if (i > 0)
                {
                    MessageBox.Show("User Added");
                    Form1 fm1 = new Form1();
                    fm1.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Error Adding User");
                }
            }catch(Exception u)
            {
                MessageBox.Show("Unknown Error Occured .");
            }
        }
    }
}
