﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassCrypt
{
    public partial class MainLogin : Form
    {
        public MainLogin()
        {
            InitializeComponent();
            
         
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-N2I1H66 ; Initial Catalog=PassBox; Integrated Security=True");
            string sql = "insert into passwordStore(passwordFor,password,username) values(@pf,@pc,@un)";
            SqlCommand con = new SqlCommand(sql, sqlCon);
            con.Parameters.AddWithValue("@pf", txtPassFor.Text);
            con.Parameters.AddWithValue("@pc", txtPassChar.Text);
            con.Parameters.AddWithValue("@un", textBox1.Text);

            sqlCon.Open();
            try
            {
                int i = con.ExecuteNonQuery();

                if (i > 0)
                {
                    MessageBox.Show("Password Added");
                }
                else
                {
                    MessageBox.Show("Error Adding Password");
                }
            }
            catch (Exception u)
            {
                MessageBox.Show("Unknown Error Occured");
            }
        }

        private void passwordVaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PasswordSafe ps = new PasswordSafe();
            ps.txtFetchUsername.Text = textBox1.Text;
            ps.Show();
        }
       

        private void MainLogin_Load(object sender, EventArgs e)
        {
        
            

        }

        private void loginLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogSave ls = new LogSave();
           ls.txtGotUser.Text = textBox1.Text;
            ls.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            DateTime currentDate = DateTime.Now;

            SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-N2I1H66 ; Initial Catalog=PassBox; Integrated Security=True");
            string sql = "insert into login_log(Date,Username)values(@d,@username)";
            SqlCommand conz = new SqlCommand(sql, sqlCon);
            conz.Parameters.AddWithValue("@d", currentDate);
            conz.Parameters.AddWithValue("@username", textBox2.Text);


            sqlCon.Open();

            conz.ExecuteNonQuery();
        }
    }
}
